/** 
 * 资源加载管理扩展
 * Author      : donggang
 * Create Time : 2016.8.1
 */

// 1.3引擎代码
cc.loader.release = function (asset) {
    if (Array.isArray(asset)) {
        AutoReleaseUtils.autoRelease(this, asset);
    }
    else if (asset) {
        var id = asset._uuid || asset;
        var item = this.getItem(id);
        if (item) {
            var removed = this.removeItem(id);
            // TODO: Audio
            asset = item.content;
            if (asset instanceof cc.Texture2D) {
                cc.textureCache.removeTextureForKey(item.url);
            }
            else if (CC_JSB && asset instanceof cc.SpriteFrame && removed) {
                // for the "Temporary solution" in deserialize.js
                asset.release();
            }
        }
    }
},

/** 
 * 加载一个组件中的资源
 * @param urls(Array)                       地址数组
 * 例：
 *      var data = [
 *          ['common/config', false],       第二个参数为 false 时，地址为一个文件
 *          ["common/persist/audio", true]  第二个参数为 true  时，地址为一个文件夹目录
 *      ];
 * @param completeCallback(function)        完成回调
 */
cc.loader.loadResByUrls = function(urls, completeCallback){
    var items = [];
    var loadSingleComplete = function(error, asset){
        if(error){
            cc.error(error.message || error);
            return;
        }

        items.push(asset);

        if (list.length > 0) {
            loadSingle(list.shift());
        }
        else if(list.length == 0) {
            if (completeCallback) completeCallback(items);
        }
    }

    var loadSingle = function(url){
        if (url instanceof Array){
            if(url[1] == true){
                cc.loader.loadResAll(url[0], loadSingleComplete);
            }
            else {
                cc.loader.loadRes(url[0], loadSingleComplete);
            }
        }
        else{
            cc.loader.loadRes(url, loadSingleComplete);
        }
    }

    var list = urls.slice(0);
    if (list.length > 0)
        loadSingle(list.shift());
}

/** 
 * 获取指定类型的资源 
 * @param url(string)   文件地址
 * @param type(object)  资源对象类型
 */
cc.loader.getResByType = function(url, type){
    var uuid = this._getResUuid(url, type);
    var item = this.getItem(uuid);

    if(item){
      return item.content;
    }

    return null;
}

/**
 * 通过具体的 url 来访问到文件内容
 * @param url(string) 文件地址
 */
cc.loader.getResByUrl = function(url){
    var fullUrl = cc.url.raw(cc.path.join("resources/", url));
    var mapItem = this.getItems().map[fullUrl];

    var uuid;

    if (mapItem) {
        uuid = mapItem.alias;
    }
    else {
        return null;
    }

    var item = this.getItem(uuid);
    
    if(item){
        return item.content;
    }

    return null;
}
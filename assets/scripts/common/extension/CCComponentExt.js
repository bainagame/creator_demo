/** 
 * 节点组件对象扩展
 * Author      : donggang
 * Create Time : 2016.11.16
 */

/**
 * 注册全局事件
 * @param event(string)      事件名
 * @param listener(function) 处理事件的侦听器函数
 * @param thisObj(object)    侦听函数绑定的this对象
 */
cc.Component.prototype.on = function(event, listener, thisObj){
    if (this._msg == null){
        this._msg = new game.Message();
    }
    this._msg.on(event, listener, thisObj);
}

/**
 * 移除全局事件
 * @param event(string)      事件名
 * @param listener(function) 处理事件的侦听器函数
 * @param thisObj(object)    侦听函数绑定的this对象
 */
cc.Component.prototype.off = function(event, listener, thisObj){
    if (this._msg){
        this._msg.on(event, listener, off);
    }
}

/** 
 * 触发全局事件 
 * @param event(string)      事件名
 * @param arg(Array)         事件参数
 */
cc.Component.prototype.dispatchEvent = function(event, arg) {
    game.dispatchEvent(event, arg);
},

//----------节点资源内存管理----------

/** 初始化资源管理环境 */
cc.Component.prototype._resManagerinit = function(){ 
    if (this._reference == null){
        this._reference = game.scene.__getReference();      // 引擎计数管理器
        this._resources = {};                               // 当前节点使用的资源集合
    }
}

/** 加载单个资源（使用方式和 cocos 自代同名 API 一样）*/
cc.Component.prototype.loadRes = function(url, type, completeCallback) {
    if (!completeCallback && type && !cc.isChildClassOf(type, cc.RawAsset)) {
        completeCallback = type;
        type             = null;
    }
    
    this._resManagerinit(); 
    this._reference.loadRes(url, type, completeCallback, this._resources);
}

    /** 加载一个文件夹多个资源（使用方式和 cocos 自代同名 API 一样）*/
cc.Component.prototype.loadResAll = function(url, type, completeCallback) {
    if (!completeCallback && type && !cc.isChildClassOf(type, cc.RawAsset)) {
        completeCallback = type;
        type             = null;
    }

    this._resManagerinit();
    this._reference.loadResAll(url, type, completeCallback, this._resources);
}

/**
 * 加载多个资源
 * @param urls(Array)                   资源地址数组
 * @param completeCallback(function)    加载完成回调
 */
cc.Component.prototype.loadResByUrls = function(urls, completeCallback) {
    this._resManagerinit();
    this._reference.loadResByUrls(urls, completeCallback, this._resources);
}

/**
 * 组件释放事件重写（如果业务层需要重写必须加个 this._super() 方面）
 * @param urls(Array)                   资源地址数组
 * @param completeCallback(function)    加载完成回调
 */
cc.Component.prototype.onDestroy = function(){
    if (this._msg) {
        this._msg.removes();
    }

    if (this._reference) {
        // cc.log("【资源】准备释放 {0} 节点相关的资源".format(this.node.name));
        this._reference.destroy(this._resources);
    }

    this.onRelease();
}

/** 节点组件释放事件 */
cc.Component.prototype.onRelease = function(){

}
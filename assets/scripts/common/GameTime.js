/** 
 * 游戏全局时刻表
 * Author      : donggang
 * Create Time : 2016.11.9
 */

/** 定时器事件 */
window.game = window.game || {};

game.TimerEvent = {
    TIME_A_SECOND : "time_a_second"                                         // 每秒跳动一次的的定时器
}; 

var GameTime = cc.Class({
    ctor : function(){
        this._scheduler = cc.director.getScheduler();
        this._isRun     = false;
    },

    start : function(){
        if (this._isRun == false){
            this._isRun = true;
            this._scheduler.schedule(this._onASecond, this, 1);              // 每秒跳动一次的的定时器
        }
    },

    stop : function(){
        if (this._isRun){
            this._isRun = false;
            this._scheduler.unschedule(this._onASecond, this);
        }
    },

    _onASecond : function(){
        game.dispatchEvent(game.TimerEvent.TIME_A_SECOND);
    }
});

game.timer = module.exports = game.timer || new GameTime();                 // 全局接口

//----------------------------------------------------------------------------------------------------

/** 获取游戏当前运行时间 */
var initTime = (new Date()).getTime();      // 当前游戏进入的时间毫秒值
game.getTime = function() {
    return game.getLocalTime() - initTime; 
};

/** 获取本地时间 */
game.getLocalTime = function(){
    return Date.now();
}
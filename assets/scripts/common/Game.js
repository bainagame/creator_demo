/** 
 * 游戏引擎入口
 * Author      : donggang
 * Create Time : 2016.7.26
 */
window.game = window.game || {};        // 引擎根路径

require("GameConfig");                  // 全局环境设置

require("MathExt");                     // 数学对象扩展
require("DateExt");                     // 日期对象扩展
require("ArrayExt");                    // 数组对象扩展
require("StringExt");                   // 字符串对象扩展

require("CCJSExt");                     // 引擎cc.js对象扩展
require("CCUrlExt");                    // 引擎cc.url对象扩展
require("CCNodeExt");                   // 引擎cc.Node对象扩展
require("CCColorExt");                  // 引擎cc.Color对象扩展
require("CCLoaderExt");                 // 引擎cc.loader对象扩展
require("CCComponentExt");              // 引擎cc.Component对象扩展
require("CCTextureCacheExt");           // 引擎cc.textureCache对象扩展

require("MessageManager");              // 游戏全局消息管理
require("PoolManager");                 // 对象池
require("GameTime");                    // 游戏时间

require("CacheManager");                // 全局缓存 
require("AudioManager");                // 音乐管理
require("SceneManager");                // 场景管理

cc.Class({
    extends: cc.Component,

    onLoad: function () {
        game.canvas = cc.find("Canvas");
        game.scene.init();

        // 游戏显示事件
        cc.game.on(cc.game.EVENT_SHOW, function () {
            game.audio.resumeMusic();
            game.audio.resumeAllEffects();
        });

        // 游戏隐藏事件
        cc.game.on(cc.game.EVENT_HIDE, function () {
            game.audio.pauseMusic();
            game.audio.pauseAllEffects();
        });

        this.run();
    },

    run : function(){
        // TODO:重写方法，进入游戏 
    }
});
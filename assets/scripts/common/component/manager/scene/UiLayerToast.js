/** 
 * 游戏系统提示信息层（游戏中间项上轮播提示）
 * Author      : donggang
 * Create Time : 2016.11.8
 */
game.UiLayerToast = module.exports = cc.Class({
    extends : require("UiLayer"),

    ctor : function () {
        this.name = game.SceneLayerType.TOAST;
        this._toastPath = 'common/persist/view/toast';
        this._prefab = null;
    },

    /**
     * 提示信息
     * @params content(string)  提示文本信息
     */
    prompt : function(content){
        if (this._prefab) {
            this._addToast(content);
        } 
        else {
            this.loadRes(this._toastPath, cc.Prefab, function (prefab) {
                this._prefab = prefab;
                game.pool.create('toast', 10, this._createAndShowToast.bind(this));
                this._addToast(content);
            }.bind(this));
        }
        
    },

    /** 
     * 增加一个toast 
     * @params content(string)  提示文本信息
    */
    _addToast: function (content) {
        var toastNode = game.pool.get('toast');
        this.addChild(toastNode);
        var toastCom = toastNode.getComponent('Toast');
        toastCom.show(content, this._toastRecovery);
    },

    /** 
     * 回收节点
     * @param node(cc.Node)    要回收的节点
     */
    _toastRecovery: function (node) {
        game.pool.put('toast', node);
    },

    /** 创建一个toast阶段 */
    _createAndShowToast: function () {
        var toastNode = cc.instantiate(this._prefab);

        return toastNode;
    },
});
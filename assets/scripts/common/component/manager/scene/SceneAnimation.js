/** 
 * 场景层类型
 * Author      : donggang
 * Create Time : 2016.11.8
 * 
 * Internal　   
 */
module.exports = {
    /** 打开场景事件遮挡 */
    blockEventOpen : function(){
        if (this._block == null){
            this._block = cc.Node.createBlockEventNode();
            cc.Node.touchStopPropagationOpen(this._block);
        } 
        this._block.parent = game.canvas;
    },

    /** 关闭场景事件遮挡 */
    blockEventClose : function(){
        if (this._block){
           this._block.parent = null;
        } 
    },

    /**
     * 创建场景动画
     * @param previou(UiLayer)             上一个界面
     * @param current(UiLayer)             当前一个界面
     * @param operate{
     *      animation : path(string)       新场景打开动画（两个动画必须同时存在）
     * }
     * @param complete(function)           动画完成事件
     */
    createAnimationByPrefab : function(previou, current, operate, complete){
        var previouCloseAnimComplete = function(event) {
            if (event.target.name == "window_show"){
                this._animation.play("window_hide");
                previou.active = false;
                current.active = true;

                if (complete) {
                    complete();
                }
            }
            else if(event.target.name == "window_hide"){
                this._animation.off("finished" , previouCloseAnimComplete);
                this._animation.off("lastframe", previouCloseAnimComplete);
                this._animationNode.parent = null;

                // 触发当前界面动画转换完成事件
                current.applyComponentsFunction("onReplaceAnimationFinished");  
            }
        }.bind(this);

        var playAnimation = function(){
            this._animationNode.parent = game.canvas;
            this._animation.on("finished" , previouCloseAnimComplete);
            this._animation.on("lastframe", previouCloseAnimComplete);
            this._animation.play("window_show");
        }.bind(this);

        if (previou && operate && operate.animationPrefab){ 
            current.active = false;

            if (this._animationNode == null){
                cc.loader.loadRes(operate.animationPrefab, cc.Prefab, function(error, prefab){
                    if (error) cc.error("名为【{0}】的预制动画不存在");

                    this._animationNode = cc.instantiate(prefab);
                    this._animation     = this._animationNode.getComponent(cc.Animation);

                    playAnimation();
                }.bind(this));
            }
            else {
                playAnimation();
            }

            return true;
        }

        return false;
    }
}
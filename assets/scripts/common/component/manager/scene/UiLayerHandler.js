/** 
 * 场景层附加功能处理队列
 * Author      : donggang
 * Create Time : 2016.11.21
 * 
 * Internal　   
 * 
 * 注：此模块可定制转场时的队列处理流程，覆盖 game.UiLayerHandler 对象即可
 */
var StateQueue = require("StateQueue");

game.UiLayerHandler = module.exports = cc.Class({
    ctor : function(){
        this._queue = new StateQueue();

        // 定制 pomelo 服务器数据请求
        this._queue.push(this._request.bind(this));

        // 预加载资源
        this._queue.push(this._preloaded.bind(this));

        // 加载前预处理逻辑
        this._queue.push(this._handler.bind(this));

        // 加载预制资源
        this._queue.push(this._loadNodePrefab.bind(this));

        // 初始化状态机
        this._queue.init();
    },

    /** 
     * 定制 pomelo 服务器数据请求
     * @param protocol 协议
     * @param params   参数
     */
    _request : function(){
        if (this._operate.request){
            game.protocol.request(this._operate.request.protocol, this._operate.request.params, function (data) {
                if (this._operate.request.callback) this._operate.request.callback(data);
                
                this._queue.next();
            }.bind(this));
        }
        else{
            this._queue.next();
        }
    },

    /** 预加载资源 */
    _preloaded : function(){
        if (this._operate.preloaded){
            if (cc.js.isString(this._operate.preloaded)) {
                this._mNode.loadResAll(this._operate.preloaded,null, function(items) { 
                    this._queue.next();
                }.bind(this));
            } 
            else if (this._operate.preloaded instanceof Array) {
                this._mNode.loadResByUrls(this._operate.preloaded, function(items) { 
                    this._queue.next();
                }.bind(this));
            }
        }
        else {
            this._queue.next();
        }
    },

    /** 加载前预处理逻辑（只处理同步业务） */
    _handler : function(){
        if (this._operate.handler){
            this._operate.handler();
            this._queue.next();
        }
        else {
            this._queue.next();
        }
    },

    /** 加载前预处理逻辑 */
    _loadNodePrefab : function(){
        this._mNode.loadRes(this._operate.resPath, cc.Prefab, function(prefab) {
            var operate = this._operate;
            var node    = cc.instantiate(prefab);
            
            // 绑定资源管理节点到预制节点上
            this._mNode.parent = node;

            // 缓存节点
            this._layer._nodes[operate.resPath] = node;             // 添加到节点集合缓存

            if (node.width == 0 || node.height == 0)
                cc.error("【场景】节点宽和高不都不能为零，请检查预制【{0}】在编辑器中根节点的Size属性".format(node.name));
            
            // 显示节点
            this._layer._show(node, operate, this._completeCallback); 

            this._queue.next();
        }.bind(this));
    },

    /**
     * 开始执行状态队列
     */
    start : function(layer, operate, completeCallback){
        this._mNode            = new cc.Node("manager_node");          // 管理资源的空节点
        this._layer            = layer
        this._operate          = operate;
        this._completeCallback = completeCallback;
        this._queue.start();
    },

    /** 结束状态队列 */
    end : function() {
        this._queue.end();
    }
})
/** 
 * 场景层类型
 * Author      : donggang
 * Create Time : 2016.11.8　　　　　　　　　　　　　　　　　   
 */

/** 场景类型 */
game.SceneType = cc.Enum({
    DEFAULT    : "scene_default",                // 默认场景    － 界面层、弹窗层、静态弹窗层、提示层
    RPG        : "scene_rpg"                     // RPG游戏场景 － 地图层、界面层、弹窗层、静态弹窗层、提示层
});

/** 场景层类型 */
game.SceneLayerType = cc.Enum({
    MAP        : "layer_map",                    // 游戏玩法层 － 用于地图滚屏
    UI         : "layer_ui",                     // 操作界面层 － 常驻界面
    POP        : "layer_pop",                    // 弹出窗口层 － 窗口获取焦点时绘制在当前逻辑层的最上层
    POP_STATIC : "layer_pop_static",             // 静态弹出窗口层
    GUIDE      : "layer_guide",                  // 新手引导层 － 引擎界面通过获取目标引导按钮的位置动态定位到目标上，避免不同屏幕出现偏差
    TIPS       : "layer_tips",                   // 提示信息层 － 悬浮指定明接点边弹出的提示层
    TOAST      : "layer_toast"                   // 提示信息层 － 中间弹出提示层
});
/** 
 * 游戏静态弹窗界面层
 * 
 * 需求：
 * 1、队列方式弹出
 * 2、当前显示窗口关闭时才会弹出下一个
 * 
 * Author      : donggang
 * Create Time : 2016.9.24
 */
game.UILayerStatic = module.exports = cc.Class({
    extends : require("UiLayer"),

    ctor : function(){
        this.name = game.SceneLayerType.POP_STATIC;

        this._block        = cc.Node.createBlock();
        this._block.parent = this;

        this.active = false;

        this._queue = [];
    },

    add : function(resPath, operate, completeCallback){
        if (this.active) {
            var data              = {};
            data.resPath          = resPath;
            data.operate          = operate;
            data.completeCallback = completeCallback;
            this._queue.push(data);
            return;
        }
        
        this.active = true;
        cc.Node.touchStopPropagationOpen(this);
        
        this._super(resPath, operate, completeCallback);
    },
    
    /** 不执行 remove 移除场景时自处理移除相关逻辑 */
    _onChildRemoved : function(event) {
        this._super(event);

        cc.Node.touchStopPropagationClose(this);
        this.active = false;

        if (this._queue.length > 0){
            var data = this._queue.shift();
            this.add(data.resPath, data.operate, data.completeCallback);
        }
    },

    clear : function(cleanup = false){
        this._queue = [];
        this._super(cleanup); 
    }
});

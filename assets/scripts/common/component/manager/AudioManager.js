/** 
 * 游戏音乐管理
 * Author      : donggang
 * Create Time : 2016.8.22
 * 
 * 需求说明
 * 1、管理背景音乐和音效的加载播放
 * 2、统一管理背景音乐和音效的音量控制
 */
var AudioManager = cc.Class({
    ctor : function () {
        this._musicVolume     = 1.0;       // 背景音乐音量
        this._effectVolume    = 1.0;       // 音效音量
        this._effects         = {};        // 音乐剪辑集合
        this._musicPlaying    = false;     // 背景音乐正在播放
        this._musicPlayFinish = null;      // 播放完成回调
        this._musicSwitch     = true;      // 音乐开关
        this._effectSwitch    = true;      // 音效开关
        this._playingMusic    = "";        // 当前播放的音乐
        this._scheduler       = cc.director.getScheduler();
        this._scheduler.schedule(this._update, this, 0.10);          // 每秒跳动一次的的定时器
        this._musicStopVol    = 0;
        this.volumeVaryNum    = 1;                                 // 声音每次次数
        this._changeDirection = 1;         // 1,声音变大;-1,声音变小
        this._changeStep      = 0.1;

    },

    _update(dt){
        if (this._musicPlaying) {
            if (this.isMusicPlaying() == false){
                this._musicPlaying = false;

                // 背景音乐播放完成事件
                if (this._musicPlayFinish)
                    this._musicPlayFinish();
            }
        }

        var volume = this.getMusicVolume();

        if ((volume > 0 && this._changeDirection == -1) || (volume < 1 && this._changeDirection == 1)) {
            this.setMusicVolume(volume + this._changeStep * this._changeDirection);
        }

        if (volume == 0 && this._changeDirection == -1) {
            this.stopMusic(this.releaseData);
            this._playingMusic = "";
        }
    },

    /**
     * 背景音乐播放完成回调
     * @param callBack(function)       回调方法
     */
    setMusicPlayFinish(callBack){
        this._musicPlayFinish = callBack;
    },

    /** 
     * 初始化音乐和音效开关
     * @param musicSwitch(bollean)       音乐开关
     * @param effectSwitch(boolean)      音效开关
     */
    initSwitch: function (musicSwitch, effectSwitch) {
        this._musicSwitch  = musicSwitch;
        this._effectSwitch = effectSwitch;
    },

    /**
     * 开关音乐
     */
    switchMusic: function () {
        this._musicSwitch = !this._musicSwitch;
        if (!this._musicSwitch) {
         
        }
    },

    /** 
     * 开关音效 
     */
    switchEffect: function () {
        this._effectSwitch = !this._effectSwitch;
        if (!this._musicSwitch) {
            this.stopAllEffects();
        }
    },

    /**
     * 获取音乐开关状态
     */
    getMusicSwitch: function () {
        return this._musicSwitch;
    },

    /**
     * 获取音效开关状态
     */
    getEffectSwitch: function () {
        return this._effectSwitch;
    },

    /** 
     * 播放指定音乐
     * @param {String}  url              音效路径
     * @param {boolean} loop             是否循环播放    
     */
    playMusic : function(url, loop = false) {
        // 如果要播放的音乐是正在播放的音乐,就跳过
        if (this._playingMusic == url) {
            this._changeDirection = 1;
            return ;
        }

        if (this._musicSwitch) {
            var rawUrl = cc.url.raw(url);
            if (cc.loader.getRes(rawUrl)){
                cc.audioEngine.playMusic(rawUrl, loop);

                this._changeDirection = 1;
                this._playingMusic = url;
            }
            else {
                cc.warn("【音频】背景音乐文件{0}不存在".format(url));
            }
        } 
    },

    /**
     * 音量递归减小,或增加
     * @param isReduce: true减小，false增大
     * @param nowVolum: 0表示增大，1表示减小
     * @param varyNum: 每次递减多少，可自定义
     */
    reduceOrAddVolume : function(isReduce, nowVolum, varyNum = false){
        var vary   = 0.1; //默认递减值
        if(varyNum){  //判断是否使用自定义值
            vary = varyNum;
        }
        if(isReduce){ // 判断音量是否是减小
            vary = -vary;
            nowVolum  += vary * this.volumeVaryNum;
        }
        else{
            nowVolum  += vary * this.volumeVaryNum;
        }
        this.volumeVaryNum++;
        if(nowVolum <= 0){
            this.volumeVaryNum = 1;
            nowVolum = 0;
        }
        if(nowVolum >= 1){
            this.volumeVaryNum = 1;
            nowVolum = 1;
        }
        return nowVolum; //返回当前音量值
    },

    /**
     * 停止当前音乐
     * @param releaseData(boole)        如果释放的音乐数据则为true, 为默认值是false
     */
    stopMusic : function(releaseData) {
        // cc.audioEngine.stopMusic(releaseData);
        this._musicRelease = releaseData;
        this._changeDirection = -1;
    },

    /** 
     * 暂停正在播放音乐
     */
    pauseMusic : function() {
        cc.audioEngine.pauseMusic();
    },

    /** 
     * 恢复音乐播放
     */
    resumeMusic : function() {
        cc.audioEngine.resumeMusic();
    },

    /** 
     * 从头开始重新播放当前音乐
     */
    rewindMusic : function() {
        cc.audioEngine.rewindMusic();
    },

    /** 
     * 获取音量 
     * return Number                   音量（0.0 ~ 1.0）
     */
    getMusicVolume : function(){
        return cc.audioEngine.getMusicVolume();
    },

    /**
     * 设置音量
     * @param volume(Number)           音量（0.0 ~ 1.0）
     */
    setMusicVolume : function(volume){
        this._musicVolume = volume;
        cc.audioEngine.setMusicVolume(volume);
    },

    /** 
     * 音乐是否正在播放（验证些方法来实现背景音乐是否播放完成）
     * return boolen
     */
    isMusicPlaying : function(){
        return cc.audioEngine.isMusicPlaying();
    },

    //------------------------------------------------------------------------------------------

    /** 
     * 播放音效
     * @param path(string)              音效路径
     */
    playEffect : function(url, loop = false) {
        if (this._effectSwitch) {
            var rawUrl = cc.url.raw(url);
            if (cc.loader.getRes(rawUrl)){
                var audio  = cc.audioEngine.playEffect(rawUrl, loop, this._effectVolume);
                this._effects[url] = audio;
            }
            else{
                 cc.warn("【音频】音效文件{0}不存在".format(url));
            }
        }
    },

    /** 
     * 获取音效音量
     * return Number                   音量（0.0 ~ 1.0）
     */
    getEffectsVolume : function(){
        return cc.audioEngine.getEffectsVolume();
    },

    /**
     * 设置音效音量
     * @param volume(Number)           音量（0.0 ~ 1.0）
     */
    setEffectsVolume : function(volume){
        this._effectVolume = volume;
        cc.audioEngine.setEffectsVolume(volume);
    },

    /** 
     * 暂停指定的音效
     * @param url(string)              音效路径
     */
    pauseEffect : function(url){
        var audio = this._effects[url];
        if (audio)
            cc.audioEngine.pauseEffect(audio);
        else
            cc.error("地址为【{0}】的音效文件不存在".format(url));
    },

    /** 
     * 暂停现在正在播放的所有音效 
     */
    pauseAllEffects : function(){
        cc.audioEngine.pauseAllEffects();
    },

    /**
     * 恢复播放指定的音效
     * @param url(string)              音效路径
     */
    resumeEffect : function(url){
        var audio = this._effects[url];
        if (audio)
            cc.audioEngine.resumeEffect(audio);
        else
            cc.error("地址为【{0}】的音效文件不存在".format(url));
    },

    /** 
     * 恢复播放所有之前暂停的所有音效
     */
    resumeAllEffects : function(){
        cc.audioEngine.resumeAllEffects();
    },

    /**
     * 停止播放指定音效
     * @param url(string)              音效路径
     */
    stopEffect : function(url){
        var audio = this._effects[url];
        if (audio)
            cc.audioEngine.stopEffect(audio);
        else
            cc.error("地址为【{0}】的音效文件不存在".format(url));
    },

    /**
     * 停止正在播放的所有音效
     */
    stopAllEffects : function(){
        cc.audioEngine.stopAllEffects();
    },

    /**
     * 卸载预加载的音效
     */
    unloadEffect : function(url){
        var rawUrl = cc.url.raw(url);
        cc.audioEngine.unloadEffect(url);
    },

    /**
     * 停止所有音乐和音效的播放
     */
    end : function(){
        cc.audioEngine.end();
    }
});

game.audio = module.exports = new AudioManager();
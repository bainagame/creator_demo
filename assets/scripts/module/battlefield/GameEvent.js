/** 场景层类型 */
module.exports = cc.Enum({
    PLAYER_ATTACK : "player_attack",    // 玩家开始攻击
    ENEMY_HIT     : "enemy_hit",        // 命中敌人
    ENEMY_ADD     : "enemy_add",        // 添加敌人
    BULLET_ADD    : "bullet_add",       // 添加子弹
});
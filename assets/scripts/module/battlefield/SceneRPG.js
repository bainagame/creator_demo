/** 
 * ＲＰＧ类游戏场景（暂不讲解 - 理解成一个游戏画面）
 * Author      : donggang
 * Create Time : 2016.9.18　　　　　　　　　　　　　　　　　　   
 */
module.exports = cc.Class({
    extends: require("SceneBase"),

    /** 场景初始化 */
    ctor : function(){
        var MapLayer = require("MapLayer");　
        　　　
        this.addChild(new MapLayer(game.SceneLayerType.MAP));
    },
});

/** 
 * 游戏玩法层 - 管理地图、飞机、子弹
 * Author      : donggang
 * Create Time : 2016.8.27
 * 
 * 需求说明
 * 1、地图滚屏
 * 2、地图上可添加的游戏元素
 */
module.exports = cc.Class({
    extends: cc.Node,

    ctor: function () {
        this._map   = new (require("Map"))();                       // 地图层
        this._layer = new cc.Node("layer_play_palyer");             // 玩家角色层

        this.addChild(this._map);
        this.addChild(this._layer);
    },

    /**
     * 创建地图
     * @param width(int)     地图宽
     * @param height(int)    地图高
     */
    createMap : function(width, height) {
        this.setContentSize(width, height);
        this._map.create(width, height);
    },

    /**
     * 添加玩家
     * @param prefabPath(string)        预制路径
     */
    createPlayer : function() {
        var prefab = cc.loader.getResByType("persist/prefab/plane_hero", cc.Prefab);
        var player = cc.instantiate(prefab);
        this._layer.addChild(player);
        return player;
    },

    /**
     * 添加敌人
     */
    createEnemy : function(id, x, y) {
        var prefab      = cc.loader.getResByType("persist/prefab/plane_enemy", cc.Prefab);
        var enemy       = cc.instantiate(prefab);
        enemy.id        = id;
        enemy.setPosition(x, y);
        this._map.addChild(enemy);
        return enemy;
    },

    /**
     * 添加子弹
     * @param targetId(int)             攻击目标编号
     */
    createBullet : function(id, x, y, direct, speed) {
        var prefab      = cc.loader.getResByType("persist/prefab/effect_bullet", cc.Prefab);
        var enemy       = cc.instantiate(prefab);
        enemy.id        = id;                     // 攻击目标编号
        enemy.x         = x;
        enemy.y         = y;
        enemy.direct    = direct;
        enemy.speed     = speed;
        this._map.addChild(enemy);
        return enemy;
    },

    /**
     * 添加子弹
     * @param targetId(int)             攻击目标编号
     */
    createEffectHit : function(targetId, x, y) {
        var prefab     = cc.loader.getResByType("persist/prefab/effect_hit", cc.Prefab);
        var enemy      = cc.instantiate(prefab);
        enemy.setPosition(x, y);
        enemy.targetId = targetId;                     // 攻击目标编号
        this._map.addChild(enemy);
        return enemy;
    }
});

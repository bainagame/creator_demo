/** 
 * 子弹命中特效
 * Author      : donggang
 * Create Time : 2016.8.27
 */
cc.Class({
    extends: cc.Component,
   
    onLoad: function () {
        this._effect = this.node.getComponent(cc.Animation);
        this._effect.on("finished", this._onFinished, this);
    },

    _onFinished : function(event){
        this.node.parent.removeChild(this.node, true);

        // game.playerComponent.reset();
    }
});

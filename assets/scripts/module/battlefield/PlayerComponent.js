/** 
 * 飞机对象
 * Author      : donggang
 * Create Time : 2016.8.27
 */
cc.Class({
    extends: cc.Component,

    properties: {
        /** 玩家名 */
        nickname: {
            set: function (value) {
                this._nickname    = value;
                this._name.string = value == undefined ? "" : value;
            }
        },

        /** 设置飞机皮肤 */
        skin: {
            set: function (value) {
                this._avatar.getComponent(cc.Sprite).spriteFrame = game.sheet.getSpriteFrame("plane{0}_0".format(value + 1));
            }
        },
    }, 

    onLoad : function () {
        this._isAddSpeed = false;       // 是否开启加速的开始变量
    },

    /** 设置飞机速度 */
    speed : function() {
        if(this._joystick == null){
            this._joystick = this.node.getComponent("Joystick");
        }

        if (this._isAddSpeed){
            this._isAddSpeed = false;
            this._joystick.setSpeed(5);
        }
        else {
            this._isAddSpeed = true;
            this._joystick.setSpeed(8);
        }
    },

    /** 死亡后回到启始位置 */
    reset : function(){
        this.node.setPosition(0, 0);
    },

    /**
     * 获取飞机机头的角度
     */
    getPlaneRotation: function(){
        return cc.find("spr_avatar", this.node).rotation;
    }
});

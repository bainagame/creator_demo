/** 
 * 战场
 * Author      : donggang
 * Create Time : 2016.8.27
 */
cc.Class({
    extends: cc.Component,

    onLoad: function () {
        // 定义游戏事件
        game.GameEvent = require("GameEvent");

        // 攻击键
        cc.find("btn_attack", this.node).on(cc.Node.EventType.TOUCH_END   , this._onTouchEnd, this);
        cc.find("btn_attack", this.node).on(cc.Node.EventType.TOUCH_CANCEL, this._onTouchEnd, this);
        
        // 加速键
        cc.find("btn_speed", this.node).on(cc.Node.EventType.TOUCH_END   , this._onTouchEnd, this);
        cc.find("btn_speed", this.node).on(cc.Node.EventType.TOUCH_CANCEL, this._onTouchEnd, this);

        // 纹理图集
        game.sheet = cc.loader.getResByType("persist/plist/sheet", cc.SpriteAtlas);
        game.play  = game.scene.getLayer(game.SceneLayerType.MAP);
        
        // 创建地图
        game.play.createMap(2000, 1000);

        // 创建飞机
        game.player = game.play.createPlayer();
        game.player.on("position-changed", function(event){                         // 飞机移动驱动地图滚屏
            game.play.x = -event.currentTarget.x;
            game.play.y = -event.currentTarget.y;
        }, this);
    
        // 飞机逻辑组件
        game.playerComponent = game.player.getComponent("PlayerComponent");

        // 游戏逻辑事件
        game.on(game.GameEvent.PLAYER_ATTACK, this._onHandler, this);
        game.on(game.GameEvent.ENEMY_HIT    , this._onHandler, this);
        game.on(game.GameEvent.ENEMY_ADD    , this._onHandler, this);
        game.on(game.GameEvent.BULLET_ADD   , this._onHandler, this);

        // 创建战斗管理器
        game.battleManager = new (require('BattleManager'))();
        game.battleManager.setPlayer(game.player);
        game.battleManager.setMapInfo(new cc.Vec2(2000, 1000));

        // 游戏开始
        game.battleManager.startGame();
    },

    _onHandler : function(event, args){
        switch(event){
            case game.GameEvent.PLAYER_ATTACK:          // 玩家开始攻击
                break;
            case game.GameEvent.ENEMY_HIT:              // 命中敌人特效
                game.play.createEffectHit(args.id, args.x, args.y); 
                break;
            case game.GameEvent.ENEMY_ADD:              // 添加敌人
                // cc.log('[BattleField _onHandler] ENEMY_ADD, x = {0}, y = {1}'.format(args.x, args.y));
                var enemy = game.play.createEnemy(args.id, args.x, args.y);
                game.battleManager.addEnemy(enemy);
                break;
            case game.GameEvent.BULLET_ADD:             // 添加子弹
                // cc.log('[BattleField _onHandler] BULLET_ADD, x = {0}, y = {1}, direct = {2}'.format(args.x, args.y, args.direct));
                var bullet = game.play.createBullet(args.id, args.x, args.y, args.direct, args.speed);
                game.battleManager.addBullet(bullet);
                break;
        }
    },

    /** 战斗技能按钮 */
    _onTouchEnd : function(event){
        switch(event.target.name){
            case "btn_attack":
                game.battleManager.createBullet();
                break;
            case "btn_speed":
                game.playerComponent.speed();           // 改变玩家飞行速度    
                break;
        }
    }
});

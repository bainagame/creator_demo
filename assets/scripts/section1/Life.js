cc.Class({
    extends: cc.Component,

    // 组件脚本的初始化阶段触发
    onLoad: function () {
        // 一般用于游戏组件逻辑的初如化工作（例：声明变量、定义对象等）
    },

    // 组件第一次激活前触发
    start: function () {
        // 初始化工作一般需要一定时间，有些时间相关处理需要在游戏 update 开始前开始计算时，在这里初始划对象
        this._timer = 0.0;
    },

    // 每一帧渲染前触发
    update: function (dt) {
        this._timer += dt;
        if ( this._timer >= 5.0 ) {
            console.log('I am done!');          // 5秒后开始打印日志
        }

        if ( this._timer >= 10.0 ) {            // 10秒后禁用组件并释放组件
            this.enabled = false;
            this.destroy();
        }
    },

    // 每一帧渲染后触发
    lateUpdate: function (dt) {
        this.node.rotation = 20;
    },

    // 当组件的 enabled 属性从 false 变为 true 时触发
    onEnable: function () {
        cc.log("onEnable");         // 组件初始化后 enabled 默认为 true 所以会触发
    },

    // 当组件的 enabled 属性从 true 变为 false 时触发
    onDisable: function () {
        cc.log("onDisable");
    },

    // 当组件调用了 destroy()时，会触发
    onDestroy: function () {
        cc.log("onDestroy");
    },
});

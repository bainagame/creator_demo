cc.Class({
    extends: cc.Component,

    properties: {
        sprite: {
            default: null,
            type: cc.SpriteFrame,
        },

        plane: {
            default: null,
            type: cc.Prefab
        }
    },

    onLoad: function () {
        // 创建新节点
        var node = new cc.Node("sprite");
        var sp   = node.addComponent(cc.Sprite);

        sp.spriteFrame = this.sprite;
        node.parent    = this.node;
        node.setPosition(0,0);


        // 克隆已有节点
        this.nodePlane = cc.instantiate(this.plane);
        this.nodePlane.setPosition(200, 0);
        this.node.addChild(this.nodePlane);
    },

    release : function(){
        this.nodePlane.destroy();
    }
});

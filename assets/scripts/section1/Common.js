cc.Class({
    extends: cc.Component,

    onLoad: function () {
        this.image = cc.find("image_01", this.node);
    },

    // 关闭/激活节点
    btnActive : function(){
        if (this.image.active){
            this.image.active = false;
        }
        else{
            this.image.active = true;
        }
    },

    // 更改节点位置
    btnPosition : function(){
        if (this.image.x == 0){
            this.image.x = 100; 
            this.image.y = 100;
        }
        else{
            this.image.position = cc.p(0, 0);
        }
    },

    // 更改节点旋转
    btnRotation : function(){
        if (this.image.rotation == 0){
            this.image.rotation = 90; 
        }
        else{
            this.image.setRotation(0);
        }
    },

    // 更改节点缩放
    btnScale : function(){
        if (this.image.scaleX == 1){
            this.image.scaleX = 2; 
            this.image.scaleY = 2; 
        }
        else{
            this.image.setScale(1, 1);
        }
    },

    // 更改节点缩放
    btnSize : function(){
        if (this.image.width == 46){
            this.image.width  = 200; 
            this.image.height = 100;
        }
        else{
            this.image.setContentSize(46, 59);
        }
    },

    // 更改节点锚点位置
    btnAnchor : function(){
        if (this.image.anchorX == 0.5){
            this.image.anchorX = 0; 
            this.image.anchorY = 0;
        }
        else {
            this.image.setAnchorPoint(0.5, 0.5);
        }
    },

    // 透明度
    btnOpacity : function(){
        if (this.image.opacity == 255){
            this.image.opacity = 100; 
        }
        else {
            this.image.opacity = 255; 
        }
    }
});

// 自定义属性
var Custom = cc.Class({
    name      : "Custom",
    properties: {
        p1: {
           default: null,
           type: cc.Node
        },
        p2: {
           default: null,
           type: cc.Node
        },
    },
});


cc.Class({
    extends: cc.Component,

    // 利用属性检查器设置节点（3）
    properties: {
        image1: {
            default : null,
            type    : cc.Node
        },
        image2: {
            default : [],               // 多个对象设置
            type    : Custom
        } 
    },

    // use this for initialization
    onLoad: function () {
        this.getThisNode();             // (1)
        this.getOtherComponent();       // (2)
        this.findChildNode();
    },

    // 获得组件所在的节点
    getThisNode : function(){
        var node = this.node;
        cc.log("x = " + node.x);
    },
    
    // 获得其它组件
    getOtherComponent : function(){
        var txt_title = cc.find("txt_title", this.node);
        var label     = txt_title.getComponent(cc.Label);        //  txt_title.getComponent("cc.Label");
        var text      = 'update';

        // 修改文字组件内容
        label.string = text;
    },

    // 查找子节点
    findChildNode : function(){
        this.cannons = this.node.getChildren();     // 获取当前节点的所有子节点数组对象
        
        for(var i = 0; i < this.cannons.length; i++){
            cc.log("ChildNodeName = " + this.cannons[i].name); 
        }

        // 查询制定名字的子节点
        var txt_title_node = this.node.getChildByName("txt_title");
        cc.log(txt_title_node.getComponent(cc.Label).string);

        // 全局名字查找
        txt_title_node = cc.find("Canvas/txt_title");
        cc.log(txt_title_node.getComponent(cc.Label).string);
    }


    // called every frame, uncomment this function to activate update callback
    // update: function (dt) {

    // },
});

cc.Class({
    extends: cc.Component,

    // use this for initialization
    onLoad: function () {
        this.image_01 = cc.find("image_01", this.node);

        this.btnOnEvent();
        
        // 发射事件监听（同级节点自定义事件监听）
        this.image_01.on("emit", this.onEmit, this);

        // 派送事件监听（通过父节点接受事件冒泡的消息）
        this.node.on("dispatch", this.onDispatch, this);
    },

    onEmit : function(event){
        console.log("发射事件触发" + event.detail.msg);
    },

    onDispatch: function(event){
        console.log("派送事件触发");
    },

    onTouchStart : function(event){
        console.log("image_01 触摸按下时触发");
    },

    onTouchEnd : function(event){
        console.log("image_01 在节点区域内触摸放开时触发");
    },

    onTouchMove : function(event){
        console.log("image_01 触摸按下时在节点区域内移动时触发");
    },

    onTouchCancel : function(event){
        console.log("image_01 在节点区域外放开时触发");
    },

    // 触摸事件监听事件
    btnOnEvent : function(){
        this.image_01.on(cc.Node.EventType.TOUCH_START , this.onTouchStart , this);         // 触摸按下时触发
        this.image_01.on(cc.Node.EventType.TOUCH_END   , this.onTouchEnd   , this);         // 在节点区域内触摸放开时触发
        this.image_01.on(cc.Node.EventType.TOUCH_MOVE  , this.onTouchMove  , this);         // 触摸按下时在节点区域内移动时触发
        this.image_01.on(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);         // 在节点区域外放开时触发
    },

    // 关闭监听
    btnOffEvent : function(){
        this.image_01.off(cc.Node.EventType.TOUCH_START , this.onTouchStart , this);
        this.image_01.off(cc.Node.EventType.TOUCH_END   , this.onTouchEnd   , this);
        this.image_01.off(cc.Node.EventType.TOUCH_MOVE  , this.onTouchMove  , this); 
        this.image_01.off(cc.Node.EventType.TOUCH_CANCEL, this.onTouchCancel, this);         

        // 发射事件
        this.image_01.emit("emit", {msg: "我点下了这个图片"});

        // 派送事件
        this.image_01.dispatchEvent(new cc.Event.EventCustom('dispatch', true));
    }
});

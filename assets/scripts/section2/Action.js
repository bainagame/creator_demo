cc.Class({
    extends: cc.Component,

    onLoad: function () {
        this.plane = cc.find("plane", this.node);
    },

    // 移动指定的距离
    btnMoveBy : function(){
        this.plane.position = cc.p(0,0);
        this.plane.runAction(cc.moveBy(1, 200, 0));
    },  

    // 旋转到目标角度
    btnRotateBy : function(){
        this.plane.position = cc.p(0,0);
        this.plane.runAction(cc.rotateBy(1, 180));
    },

    // 顺序动作
    btnSequence : function(){
        this.plane.position = cc.p(0,0);
        var seq = cc.sequence(cc.moveBy(1, 200, 0), cc.rotateBy(1, 180));
        this.plane.runAction(seq);
    },

    // 同步动作
    btnSpawn : function(){
        this.plane.position = cc.p(0,0);
        var spawn = cc.spawn(cc.moveBy(1, 200, 0), cc.rotateBy(1, 180));
        this.plane.runAction(spawn);
    },

    // 动作回调
    btnCallFunc : function(){
        this.plane.position = cc.p(0,0);
        var seq = cc.sequence(cc.rotateBy(1, 90), cc.callFunc(function(){
            cc.log("回调动作触发");
        }));
        this.plane.runAction(seq);
    },

    // 缓动动作(缓慢加速)
    btnEasing : function(){
        var aciton = cc.scaleTo(0.5, 2, 2);
        aciton.easing(cc.easeIn(3.0));
        this.plane.runAction(aciton);
    }
});

cc.Class({
    extends: cc.Component,

    onLoad: function () {
        cc.loader.loadRes("demo/plane", function (err, prefab) {
            // var newNode = cc.instantiate(prefab);   
            var newNode = cc.instantiate(cc.loader.getRes("demo/plane"));
            this.node.addChild(newNode);
        }.bind(this));
    },
});

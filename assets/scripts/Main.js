/** 
 * 游戏入口
 * Author      : donggang
 * Create Time : 2016.8.10
 */
var Game = require("Game");

cc.Class({
    extends: Game,
    
    run : function(){
        game.scene.setSceneType("SceneRPG");
        game.scene.replaceScene("persist/prefab/battlefield", {preloaded:"persist"}); 
    }
});